CC=gcc
prefix=/usr/local
DESTDIR=
TARGET=si

FILES=si.c mit.c mm.c buddylist.c

include config.mk

all: options si

options:
	@echo si build options:
	@echo "LIBS     = ${LIBS}"
	@echo "INCLUDES = ${INCLUDES}"
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

si: si.o 
	${CC} si.o -o si ${LDFLAGS}

si.o: ${FILES}
	${CC} -c ${CFLAGS} ${TARGET}.c
clean:
	rm -f ${TARGET} *~ *.o *core 
install: all
	@mkdir -p ${DESTDIR}${DOCDIR}
	@mkdir -p ${DESTDIR}${BINDIR}
	@mkdir -p ${DESTDIR}${MAN1DIR}

	@install -d ${DESTDIR}${BINDIR} ${DESTDIR}${MAN1DIR}
	@install -m 644 COPYING README FAQ contrib/buddylist ${DESTDIR}${DOCDIR}
	@install -m 775 si ${DESTDIR}${BINDIR}
	@install -m 444 si.1 ${DESTDIR}${MAN1DIR}
	@echo "installed si"

uninstall: 
	@rm -f ${DESTDIR}${MAN1DIR}/si.1
	@rm -rf ${DESTDIR}${DOCDIR}
	@rm -f ${DESTDIR}${BINDIR}/si
	@echo "uninstalled si"

dist: clean
	@mkdir -p si-${VERSION}/contrib
	@cp -R Makefile README COPYING FAQ config.mk si.c si.1 buddylist.c mit.c mm.c si-${VERSION}
	@cp -R contrib/buddylist contrib/multitailrc si-${VERSION}/contrib
	@tar -cf si-${VERSION}.tar si-${VERSION}
	@gzip si-${VERSION}.tar
	@rm -rf si-${VERSION}
	@echo created distribution si-${VERSION}.tar.gz
