/*
 * si - silc improved - minimalistic silc client
 *
 * Copyright (C) 2006 Christian Dietrich <stettberger@gmx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

/* Buddylist stuff */

#define MAX_CONNS 20

typedef struct BuddyStruct {
  SilcClientEntry client;
  struct BuddyStruct *next;
} *Buddy;

Buddy buddies=NULL;
int buddy_conns[MAX_CONNS];
int buddyfd=0;

SILC_TASK_CALLBACK(si_handle_buddyfd_input);
void send_buddy_list(int fd);

void add_pubkey_callback(SilcClient client, SilcClientConnection conn,
                         SilcStatus status, SilcDList clients,
			 void *context);

void 
refresh_buddylist() 
{
  int i;
  for (i=0; i < MAX_CONNS; i++){
    if(!buddy_conns[i])
      continue;
    write(buddy_conns[i], "update\n", 7);
  }
  
}

void 
add_buddy(SilcClientEntry client) 
{
  Buddy new = calloc(1, sizeof (Buddy)), tmp;
  NULL_TEST(new);
  new->client = client;
  silc_client_ref_client(silc_client->client, silc_client->conn, client);
  for (tmp = buddies; tmp; tmp = tmp->next)
    if (tmp->client == client)
      return;
  DEBUG("New Buddy on List: %s", client->nickname);
  if(buddies) {
    for (tmp = buddies; tmp->next; tmp = tmp->next);
    tmp->next = new;
  }
  else 
    buddies = new;
}

void 
rm_buddy(SilcClientEntry client)
{
  Buddy del, tmp;
  if (buddies && buddies->client == client){
    buddies = buddies->next;
    return;
  }
  for(del = buddies; del && del->next; del = del->next)
    if(del->next->client == client){
      tmp = del->next;
      del->next = del->next->next;
      free(tmp);
      return;
    }
}

void 
add_pubkey(char *pubkey) 
{
  SilcBuffer attr=NULL;
  SilcAttributeObjPk obj;
  SilcPublicKey pk;
  /* Request all attributes except PUBKEY */
  attr = silc_client_attributes_request(SILC_ATTRIBUTE_DEVICE_INFO, 0);

  /* Load the Public Key */
  if (!silc_pkcs_load_public_key(pubkey, &pk)) { 
    print_out("", "-!- Couldn't load public key: %s", pubkey);
    return;
  }
  obj.type = "silc-rsa";
  obj.data = silc_pkcs_public_key_encode(pk, &obj.data_len);
  
  attr = silc_attribute_payload_encode(attr,
					SILC_ATTRIBUTE_USER_PUBLIC_KEY,
					SILC_ATTRIBUTE_FLAG_VALID,
					&obj, sizeof(obj));
  silc_client_get_clients_whois(silc_client->client, silc_client->conn,
				NULL, NULL, attr, add_pubkey_callback, NULL);
  /* Add a watch */
  char *tmp;
  silc_asprintf(&tmp, "+%s", pubkey);
  silc_client_command_call(silc_client->client, silc_client->conn, NULL, 
                           "WATCH", "-add", tmp, NULL);
  silc_free(tmp);
}

void 
add_pubkey_callback(SilcClient client, SilcClientConnection conn,
                    SilcStatus status, SilcDList clients,
                    void *context)
{
  SilcClientEntry entry;
  if (!clients) return;
  silc_dlist_start(clients);
  while ((entry = (SilcClientEntry)silc_dlist_get(clients))
         != SILC_LIST_END)  
      add_buddy(entry);
}

void 
update_buddylist() 
{
  /* Update the buddylist itself by the Pubkey's in $HOME/.silc/buddylist*/
  struct dirent *entry;
  char *path;
  silc_asprintf(&path, "%s/.silc/buddylist", (char *)getenv("HOME"));
  create_dirtree(path);
  DIR *dir = opendir(path);
  while((entry = readdir(dir))) {
    if(entry->d_name[0] == '.')
      continue;
    char *pubkey;
    silc_asprintf(&pubkey, "%s/%s", path, entry->d_name);
    add_pubkey(pubkey);
    silc_free(pubkey);
  }
  closedir(dir);
  free(path);
}

void
init_buddylist() 
{

  struct sockaddr_un strAddr;
  socklen_t lenAddr;
  int yes=1;
  int i=0;

  while(i < MAX_CONNS){
    buddy_conns[i] = 0;
    i++;
  }
  
  update_buddylist();
  /* Buddyfd */
  char *path;
  silc_asprintf(&path,"%s/buddylist", ircdir);
  unlink(path);
  if ((buddyfd = socket(PF_UNIX, SOCK_STREAM,0)) == -1) {
    perror("si: couldn't initalize buddy socket (socket)");
    return;
  }
  if (setsockopt(buddyfd, SOL_SOCKET, SO_REUSEADDR,
		 (char *) &yes, sizeof(yes)) < 0) {
    perror("si: couldn't initalize buddy socket (setsockopt)");
    return;
  }
  strAddr.sun_family = AF_UNIX; /* Unix Domain */
  strcpy(strAddr.sun_path, path);
  lenAddr = sizeof(strAddr.sun_family)+strlen(strAddr.sun_path);
  silc_free(path);

  if (bind(buddyfd, (struct sockaddr*)&strAddr, lenAddr) < 0) {
    perror("si: couldn't initalize buddy socket (bind)");
    return;
  }
  if (listen(buddyfd, MAX_CONNS) != 0){
    perror("si: couldn't initalize buddy socket (listen)");
    return;
  }
  silc_schedule_task_add_fd(silc_client->client->schedule, buddyfd,
			 si_handle_buddyfd_input, &strAddr);
}

SILC_TASK_CALLBACK(si_handle_buddyfd_input) 
{
  int conn=0;
  socklen_t lenAddr;
  int i;
  struct sockaddr_un *addr = context;
  lenAddr = sizeof(addr->sun_family) + strlen(addr->sun_path);
  if((conn = accept(fd, (struct sockaddr *) addr, &lenAddr))<0) {
    perror("si: couldn't accept connection on buddylist");
    return;
  }
  for(i = 0; i < MAX_CONNS; i++)
    if(! buddy_conns[i]){
      buddy_conns[i] = conn;
      break;
    }
  DEBUG("New Connection on Buddylist");
}

void 
si_handle_buddy_conn_input(int fd) 
{
  int pos = 0, len = 1;
  char buf[1], *data = emalloc(len);
  int count=0;
  data[0]=0;
  while(buf[0] != '\n' && (count = read(fd, buf, 1)) > 0){
    len += count;
    data = realloc(data, len);
    NULL_TEST(data);
    data = strcat(data, buf);
  }
  if(count <= 0) {
    DEBUG("Closed connection on Buddylist");
    pos=0;
    while(pos < MAX_CONNS) {
      if(buddy_conns[pos] == fd)
	buddy_conns[pos] = 0;
      pos++;
    }
    close(fd);
    return;
  }
  char *p = strchr(data, '\n');
  p[0] = 0;
  /* Proc data */
  if(! strncmp("getbuddylist", data, 12)) 
    send_buddy_list(fd);

  silc_free(data);
}

void 
send_buddy_list(int fd) 
{
  Buddy tmp;
  char *umode, *line;
  for(tmp = buddies; tmp; tmp = tmp->next) {
    umode = silc_client_umode(tmp->client->mode);
    silc_asprintf(&line, "%s,%s\n", umode, tmp->client->nickname);
    write(fd, line, strlen(line));
    silc_free(line);
    silc_free(umode);
    fsync(fd);
  }
  write(fd, "END,nouser\n", 11);
  fsync(fd);
}
