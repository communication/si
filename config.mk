# customize to fit your system

# paths
PREFIX      = /usr/local
BINDIR      = ${PREFIX}/bin
MANDIR      = ${PREFIX}/share/man
MAN1DIR     = ${MANDIR}/man1
DOCDIR      = ${PREFIX}/share/doc/si

# # includes and libs
INCLUDES    = `pkg-config --cflags silc silcclient` 
LIBS        = `pkg-config --libs silc silcclient`

VERSION=0.3

## change DESTDIR to install to a different root
DESTDIR =

# # compiler
CC          = gcc
CFLAGS      = -ggdb -O0 -W ${INCLUDES} -DVERSION=\"${VERSION}\" -DSILC_DIST_SKR -DSILC_DIST_TMA 
LDFLAGS     = ${LIBS}
