/*

  mm.c

  Author: Pekka Riikonen <priikone@silcnet.org>

  Copyright (C) 2006 Pekka Riikonen
  Copyright (C) 2007 Stefan Siegl <stesie@brokenpipe.de>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

*/

/* Multimedia routines */

SILC_TASK_CALLBACK(si_handle_data_input);

/* MIME fragment assembler */
SilcMimeAssembler mimeass = NULL;

/* Process incoming MIME message */

static void
si_process_mime(SilcClient client, SilcClientConnection conn,
		SilcClientEntry sender, SilcChannelEntry channel,
		SilcMessagePayload payload, SilcChannelPrivateKey key,
		SilcMessageFlags flags, SilcMime mime, bool recursive)
{
  const char *type;
  const unsigned char *data;
  SilcUInt32 data_len;
  SIMime simime = NULL;
  Channel *c;
  Query *q;
  static char outfile[1024], tmp[1024];
  int ret;

  if (!mime)
    return;

  /* Check for fragmented MIME message */
  if (silc_mime_is_partial(mime)) {
    if (!mimeass)
      mimeass = silc_mime_assembler_alloc();

    /* Defragment */
    mime = silc_mime_assemble(mimeass, mime);
    if (!mime)
      /* More fragments to come */
      return;

    /* Process the complete message */
    si_process_mime(client, conn, sender, channel, payload, key,
		    flags, mime, FALSE);
    return;
  }

  /* Check for multipart message */
  if (silc_mime_is_multipart(mime)) {
    SilcMime p;
    const char *mtype;
    int is_alternative;
    SilcDList parts = silc_mime_get_multiparts(mime, &mtype);

    /* Only "mixed" type supported */
    is_alternative = strcmp(mtype, "alternative") == 0;
    if (!is_alternative && strcmp(mtype, "mixed"))
      goto out;

    silc_dlist_start(parts);
    while ((p = silc_dlist_get(parts)) != SILC_LIST_END) {
      /* Recursively process parts */
      si_process_mime(client, conn, sender, channel, payload,
		      key, flags, p, TRUE);
      
      /* Content-type set to multipart/alternative,
       * handle the first alternative only */
      if(is_alternative)
        break;
    }
    goto out;
  }

  /* Get content type and MIME data */
  type = silc_mime_get_field(mime, "Content-Type");
  if (!type)
    goto out;
  data = silc_mime_get_data(mime, &data_len);
  if (!data)
    goto out;

  /* Process according to content type */

  /* Plain text */
  if (strstr(type, "text/plain")) {
    /* Default is UTF-8, don't check for other charsets */
    if (!strstr(type, "utf-8"))
      goto out;

    if (channel)
      silc_channel_message(client, conn, sender, channel, payload, key,
			   SILC_MESSAGE_FLAG_UTF8, data, data_len);
    else
      silc_private_message(client, conn, sender, payload,
			   SILC_MESSAGE_FLAG_UTF8, data, data_len);
    goto out;
  }

  /* Process supported types */

  if (channel) {
    c = channel->context;

    /* Check if we support this MIME type */
    for (simime = c->mimes; simime; simime = simime->next) {
      const char *mtype = silc_mime_get_field(simime->mime, "Content-Type");
      if (!strcmp(mtype, type))
	break;
    }
    if (!simime)
      goto out; /* Unsupported MIME type */

    if (simime->out_fd < 0) {
      memset(tmp, 0, sizeof(tmp));
      snprintf(tmp, sizeof(tmp) - 1, "out-%s", type);
      if (strrchr(tmp, '/'))
	*strrchr(tmp, '/') = '-';
      create_filepath(outfile, sizeof(outfile), channel->channel_name, tmp);
    }

  } else {
    q = sender->context;

    /* Check if we support this MIME type */
    for (simime = q->mimes; simime; simime = simime->next) {
      const char *mtype = silc_mime_get_field(simime->mime, "Content-Type");
      if (!strcmp(mtype, type))
	break;
    }
    if (!simime)
      goto out; /* Unsupported MIME type */

    if (simime->out_fd < 0) {
      memset(tmp, 0, sizeof(tmp));
      snprintf(tmp, sizeof(tmp) - 1, "%s/out-%s", sender->nickname, type);
      if (strrchr(tmp, '/'))
	*strrchr(tmp, '/') = '-';
      create_filepath(outfile, sizeof(outfile), "query", tmp);
    }
  }

  if (simime->out_fd < 0) {
    int flags = O_WRONLY;

    /* Open FIFO for writing */
    if (access(outfile, F_OK) == -1)
      mkfifo(outfile, S_IRWXU);

    if (simime->buffer == FALSE)
      flags |= O_NONBLOCK;
    simime->out_fd = open(outfile, flags, 0);
    if (simime->out_fd < 0)
      goto out;
  }

  /* Write to FIFO */
  while (data_len > 0) {
    ret = write(simime->out_fd, data, data_len);
    if (ret <= 0) {
      if (errno == EPIPE) {
	close(simime->out_fd);
	simime->out_fd = -1;
      }
      break;
    }
    data += ret;
    data_len -= ret;
  }

 out:
  if (!recursive)
    silc_mime_free(mime);
}

/* Creates a data input FIFO */

static int si_open_data_fifo(char *path, char *suffix, void *context)
{
  char infile[1024], tmp[32];
  int fd;

  memset(infile, 0, sizeof(infile));
  memset(tmp, 0, sizeof(tmp));

  snprintf(tmp, sizeof(tmp) - 1, "in-%s", suffix);
  if (strrchr(tmp, '/'))
    *strrchr(tmp, '/') = '-';
  create_filepath(infile, sizeof(infile), path, tmp);

  /* Open FIFO */
  if (access(infile, F_OK) == -1)
    mkfifo(infile, S_IRWXU);
  fd = open(infile, O_RDONLY | O_NONBLOCK, 0);

  /* Add to scheduler */
  if (fd != -1)
    silc_schedule_task_add_fd(silc_client->client->schedule, fd,
			   si_handle_data_input, context);

  return fd;
}

/* Task callback to handle data input from FIFO */

SILC_TASK_CALLBACK(si_handle_data_input)
{
  SIMime simime = context;
  Channel *c;
  Query *q;
  unsigned char buf[0xffff - 512];
  unsigned char *data;
  SilcUInt32 data_len;
  int ret;

  ret = read(fd, buf, sizeof(buf));
  if (ret <= 0) {
    if (errno == EPIPE) {
      /* Writer went away, close and reopen FIFO */
      close(fd);
      silc_schedule_unset_listen_fd(silc_client->client->schedule, fd);
      silc_schedule_task_del_by_fd(silc_client->client->schedule, fd);
      if (simime->type == CHANNEL) {
	c = simime->context;
	si_open_data_fifo(c->silc->channel_name,
			  (char *)silc_mime_get_field(simime->mime,
						      "Content-Type"),
			  context);
      } else {
	q = simime->context;
	snprintf((char *)buf, sizeof(buf) - 1, "query/%s", q->silc->nickname);
	si_open_data_fifo((char *)buf, (char *)silc_mime_get_field(simime->mime,
							   "Content-Type"),
			  context);

      }
    }
    return;
  }

  /* Encode the data */
  silc_mime_add_data(simime->mime, buf, ret);
  data = silc_mime_encode(simime->mime, &data_len);
  if (!data)
    return;

  /* Send the MIME data */
  if (simime->type == CHANNEL) {
    c = simime->context;
    silc_client_send_channel_message(silc_client->client,
				     silc_client->conn, c->silc,
                                     NULL, SILC_MESSAGE_FLAG_DATA,
				     silc_client->hash, data, data_len);
  } else {
    q = simime->context;
    silc_client_send_private_message(silc_client->client,
				     silc_client->conn, q->silc,
				     SILC_MESSAGE_FLAG_DATA, 
                                     silc_client->hash,
                                     data, data_len);
  }

  silc_free(data);
}

/* Open data FIFO for input */

static void si_open_mime(void *context, ContextType type, char *mimetype,
			 bool buffer)
{
  SIMime simime, mimes;
  Channel *c;
  Query *q;
  int fd;
  char path[1024];

  memset(path, 0, sizeof(path));
  if (type == CHANNEL) {
    c = context;
    mimes = c->mimes;
    snprintf(path, sizeof(path) - 1, "%s", c->silc->channel_name);
  } else {
    q = context;
    mimes = q->mimes;
    snprintf(path, sizeof(path) - 1, "query/%s", q->silc->nickname);
  }

  DEBUG("Input: MIME %s buffered=%s", mimetype, buffer ? "yes" : "no");

  for (simime = mimes; simime; simime = simime->next) {
    const char *mtype = silc_mime_get_field(simime->mime, "Content-Type");

    if (!strcmp(mtype, mimetype)) {
      if (simime->buffer != buffer && simime->out_fd != -1) {
	close(simime->out_fd);
	simime->out_fd = 1;
      }
      simime->buffer = buffer;

      /* Already handled */
      return;
    }
  }

  simime = silc_calloc(1, sizeof(*simime));
  if (!simime) {
    perror(("si: cannot open FIFO, out of memory"));
    return;
  }
  simime->out_fd = -1;
  simime->buffer = buffer;
  simime->type = type;
  simime->context = context;
  simime->mime = silc_mime_alloc();
  if (!simime->mime) {
    perror(("si: cannot open FIFO, out of memory"));
    silc_free(simime);
    return;
  }
  silc_mime_add_field(simime->mime, "MIME-Version", "1.0");
  silc_mime_add_field(simime->mime, "Content-Type", mimetype);
  silc_mime_add_field(simime->mime, "Content-Transfer-Encoding", "binary");

  fd = si_open_data_fifo(path, mimetype, simime);
  if (fd < 0) {
    perror(("si: cannot open FIFO"));
    silc_mime_free(simime->mime);
    silc_free(simime);
    return;
  }

  if (type == CHANNEL) {
    c = context;
    if (!c->mimes)
      c->mimes = simime;
    else {
      simime->next = c->mimes;
      c->mimes = simime;
    }
  } else {
    q = context;
    if (!q->mimes)
      q->mimes = simime;
    else {
      simime->next = q->mimes;
      q->mimes = simime;
    }
  }
}
