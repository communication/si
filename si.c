/*
 * si - silc improved - minimalistic silc client
 *
 * Copyright (C) 2006 Christian Dietrich <stettberger@gmx.de>
 * Copyright (C) 2006 Stefan Siegl <stesie@brokenpipe.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include <silc.h>	/* Mandatory include for SILC applications */
#include <silcclient.h>		/* SILC Client Library API */
#include <silcmime.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <getopt.h>
#include <dirent.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/un.h>

/* {{{  Structs, Globals Declarations */

#define NULL_TEST(a) if(!(a)){perror("si: Couldn't allocate memory"); return;}
#define NULL_TEST_ARG(a,b) if(!(a)){perror("si: Couldn't allocate memory"); return (b);}

/* This is context for our client */
enum FileTransferStatus {
  FILE_STATUS_WAIT,
  FILE_STATUS_RUN,
  FILE_STATUS_COMP,
  FILE_STATUS_ERR
};

enum FileTransferType {
  FILE_TYPE_IN,
  FILE_TYPE_OUT
};

enum PublicKeyStatus {
  KEY_UNKOWN,
  KEY_INVALID,
  KEY_TRUSTED
};

enum RemoteSide {
  SERVER,CLIENT
};

typedef enum {
  CHANNEL,
  QUERY
} ContextType;

typedef struct {
  SilcClient client;		/* The actual SILC Client */
  SilcClientConnection conn;	/* Connection to the server */
  /* Config */
  int sign:1;
  int detach_on_signal:1;
  int auto_away:1;
  SilcPublicKey pubkey;
  SilcPrivateKey privkey;
  SilcHash hash;
} *Silc;

/* Mime context */
typedef struct SIMimeStruct {
  int out_fd;
  int buffer;
  SilcMime mime;
  ContextType type;
  void *context;
  struct SIMimeStruct *next;
} *SIMime;

typedef struct Channel Channel;
struct Channel {
  int fd;
  SilcChannelEntry silc;
  SIMime mimes;
  Channel *next;
};

typedef struct Query Query;
struct Query {
  int fd;
  SilcClientEntry silc;
  SIMime mimes;
  Query *next;
};

typedef struct Transfer Transfer;
struct Transfer {
  int number;
  int type;
  int status;
  long starttime;
  float percent;

  SilcUInt32 id;
  SilcClientEntry silc;

  Transfer *next;
};

char *prefix = "";
char *username = NULL;
SilcClientEntry resolve_name = NULL;
char *realname = NULL;
char *host = "sauna.silcnet.org";
char *init_script = NULL;
int  port = 706;
char *ircdir = NULL;
Channel *channels = NULL;
Query *queries = NULL;
Transfer *transfers = NULL;
int debug = 0;
int connected = 0;
SilcClientOperations ops;
Silc silc_client;
time_t last_input;
int transfer_count = 1;


void DEBUG(char *fmt, ...);
int LocalToUtf8 (char *from, char **to);
int Utf8ToLocal (char *from, char **to);
void file_transfer(SilcClient client,
		   SilcClientConnection conn,
		   SilcClientMonitorStatus status,
		   SilcClientFileError error,
		   SilcUInt64 offset,
		   SilcUInt64 filesize,
		   SilcClientEntry client_entry,
		   SilcUInt32 session_id,
		   const char *filepath,
		   void *context);
char channel_user_mode(SilcClientEntry client, SilcChannelEntry channel);
void proc_channels_input(Channel *c, char *buf);
void proc_queries_input (Query *peer, char *buf );
static void print_out(char *channel, char *buf, ...);
char *silc_client_umode(SilcUInt32 mode);
void run_once();
SilcClientEntry silc_client_get_client_entry(const char *nick);
void silc_connection_cb(SilcClient client,
                        SilcClientConnection conn,
                        SilcClientConnectionStatus status,
                        SilcStatus error,
                        const char *message,
                        void *context);
static void
silc_channel_message(SilcClient client, SilcClientConnection conn,
		     SilcClientEntry sender, SilcChannelEntry channel,
		     SilcMessagePayload payload,
		     SilcChannelPrivateKey key,
		     SilcMessageFlags flags, const unsigned char *content,
		     SilcUInt32 message_len);
static void
silc_private_message(SilcClient client, SilcClientConnection conn,
		     SilcClientEntry sender, SilcMessagePayload payload,
		     SilcMessageFlags flags,
		     const unsigned char *msg,
		     SilcUInt32 message_len);
void *emalloc(size_t size);

/* All Copied Functions from ii */
#include "mit.c"
#include "mm.c"
#include "buddylist.c"

/* }}} */
/* {{{ Usage & Version*/
static void usage()
{
	fprintf(stderr, "%s",
			"si - silc improved - " VERSION "\n"
			"(C)opyright 2006 Christian Dietrich\n"
			"usage: si [-hvdF] [-i <silc dir>] [-s <host>] [-p <port>]\n"
			"          [-n <nick>] [-f <fullname>] [-x <init-script>]\n");
	exit(EXIT_SUCCESS);
}
static void version() {
  fprintf(stderr, "%s","si - silc improved - " VERSION "\n");
  exit(EXIT_SUCCESS);
}
/* }}} */
/* {{{ unimportant (Debug, stubs) */

void DEBUG(char *fmt, ...)
{
  if(! debug) return;
  va_list va;
  va_start(va, fmt);
  vfprintf(stderr, fmt, va);
  putchar('\n');
  va_end(va);
}

void *emalloc(size_t size) {
  void *ptr = malloc(size);
  if(!ptr) {
    perror("malloc");
    exit(EXIT_FAILURE);
  }
  return ptr;
}

/* }}} */
/* {{{ print_users */

void print_users(SilcChannelEntry channel) 
{
  int len = 0;
  char *user, *mode, *msg = emalloc(1);
  NULL_TEST(msg);
  msg[0]=0;
  SilcClientEntry user_entry;
  SilcChannelUser cuser;
  SilcHashTableList ch;

  silc_hash_table_list(channel->user_list, &ch);
  while (silc_hash_table_get(&ch, (void *)&user_entry, (void *)&cuser)) {
    mode = silc_client_chumode_char(cuser->mode);
    silc_asprintf(&user, "[%s%s] ",  mode ? mode : "", user_entry->nickname);
    len += strlen(user_entry->nickname) + strlen(mode ? mode : "")+4;
    msg = realloc(msg, len);
    msg = strcat(msg, user);

    silc_free(user);
    silc_free(mode);
  }
  print_out(channel->channel_name, msg);
  silc_free(msg);
}

/* }}} */
/* {{{ sig_handler */
void 
sig_handler(int sig)
{
  if(sig == SIGINT || sig == SIGTERM ){
    if(silc_client->detach_on_signal)
      silc_client_command_call(silc_client->client, silc_client->conn, "detach", NULL);
    else
      silc_client_command_call(silc_client->client, silc_client->conn, "quit Leaving", NULL);
  }
  silc_schedule_unset_listen_fd(silc_client->client->schedule, buddyfd);
  silc_schedule_task_del_by_fd(silc_client->client->schedule, buddyfd);
  close(buddyfd);
  buddyfd = 0;
  int i;
  for (i = 0; i < MAX_CONNS; i++){
    if(!buddy_conns[i])
      continue;
    close(buddy_conns[i]);
    buddy_conns[i] = 0;
  }  
  char *fd;
  silc_asprintf(&fd, "%s/buddylist", ircdir);
  unlink(fd);
  free(fd);
}

/* }}} */
/* {{{ UTF-8 <-> Local */
int 
LocalToUtf8 (char *from, char **to) 
{
  /* Convert Char * to Utf-8 */
  int len = silc_utf8_encoded_len((unsigned char *)from, strlen(from),
				  SILC_STRING_LOCALE);
  *to = calloc(len + 1, sizeof(**to));
  NULL_TEST_ARG(*to,FALSE);
  silc_utf8_encode((unsigned char *)from, strlen(from), SILC_STRING_LOCALE,
		   (unsigned char *)*to, len);
  return len;
}

int
Utf8ToLocal (char *from, char **to) 
{
  /* Convert Char * to Local */
  int len = silc_utf8_decoded_len((unsigned char *)from, strlen(from),
				  SILC_STRING_LOCALE);
  *to = calloc(len + 1, sizeof(**to));
  NULL_TEST_ARG(*to,FALSE);
  silc_utf8_decode((unsigned char *)from, strlen(from), SILC_STRING_LOCALE,
		   (unsigned char *)*to, len);
  return len;
}

/* }}} */
/* {{{ silc_localip */
char *
silc_localip(void)
{
  if (silc_client->conn) {
    SilcSocket socket;
    SilcStream stream;
    char *ip;
    /* Yes SilcToolkit 1.1 is much easier than 1.0 */
    stream = silc_packet_stream_get_stream(silc_client->conn->stream);
    silc_socket_stream_get_info(stream, &socket, NULL, NULL, NULL);
    silc_net_check_local_by_sock(socket, NULL, &ip);
    return ip;
  } else return NULL;
}
/* }}} */
/* {{{ proc_channels_input */
void 
proc_channels_input(Channel *c, char *buf) 
{
  char *msg;
  int len = LocalToUtf8(buf, &msg);
  Channel *ch;
  /* Commands */
  if(buf[0]=='/') {
    /* Join */
    if(buf[1]=='j' && buf[2] == ' '){
      DEBUG("Input: JOIN");
      for(ch = channels; ch; ch = ch->next)
	if(! strcmp(&buf[3], ch->silc->channel_name))
	  return; /* Already handled */
      silc_client_command_call(silc_client->client, silc_client->conn, NULL, 
              "JOIN", lower(&buf[3]), NULL);
    }
    /* ChanInfo */
    else if(! strncmp(&buf[1], "chaninfo", 8)) {
        if(c->silc->channel_name[0] == 0){
            print_out("", "-!?- The master channel is not a real channel");
        }
        else {
            if(strlen(&buf[9]) > 2) {
                if (! strncmp(&buf[10], "topic", 5)) {
                    print_out(c->silc->channel_name, "-?!- Topic: %s",
                            c->silc->topic?c->silc->topic:"None");
                }
                else if (!strncmp(&buf[10], "mode", 4)) {
                    char *str;
                    str = silc_client_chmode(c->silc->mode, c->silc->cipher,
                                             c->silc->hmac);
                    print_out(c->silc->channel_name, "-?!- Channel Mode: %s", 
                            str ? str : "None");
                    silc_free(str);
                }
                else 
                    print_out(c->silc->channel_name, 
                            "-!- Error: /chaninfo [<topic/mode>]");
            }
            else {
                char *str;
                str = silc_client_chmode(c->silc->mode, c->silc->cipher,
                                         c->silc->hmac);
                print_out(c->silc->channel_name, "-?!- Name: %s",  
                        c->silc->channel_name);
                print_out(c->silc->channel_name, "-?!- Topic: %s",
                        c->silc->topic ? c->silc->topic : "None");
                print_out(c->silc->channel_name, "-?!- Channel Mode: %s", 
                        str ? str : "None");
                silc_free(str);
            }
        }
    }
    /* Leave */
    else if (buf[1]=='l' &&  ( buf[2] == ' ' || strlen(buf)==2)) {
      DEBUG("Input: LEAVE");
      if ( buf[2] == ' ') {
	silc_client_command_call(silc_client->client, silc_client->conn, NULL, 
                "LEAVE", &buf[3], NULL);
      }
      else if(strlen(buf) == 2)
	if (strlen(c->silc->channel_name) != 0) {
	  silc_client_command_call(silc_client->client, silc_client->conn, 
                  NULL, "LEAVE", c->silc->channel_name, NULL);
	}
    }
    else if ((buf[1] ==  'n' && buf[2] == ' ') || !strcmp(&buf[1], "nick")) {
      DEBUG("Input: NICK");
      buf=strchr(&buf[1], ' ')+1;
      silc_client_command_call(silc_client->client, silc_client->conn, NULL, 
                               "NICK", buf, NULL);
    }
    else if (!strncmp("update", &buf[1],6)) {
      update_buddylist();
      refresh_buddylist();
    }
    else if (!strncmp("me ", &buf[1], 3) && strlen(&buf[1])>3){
      DEBUG("Input: ACTION");
      if (strlen(c->silc->channel_name) != 0) {
	silc_client_send_channel_message(silc_client->client, silc_client->conn, 
                                         c->silc, NULL, SILC_MESSAGE_FLAG_UTF8
					 | SILC_MESSAGE_FLAG_ACTION
					 | (silc_client->sign ? 
                                            SILC_MESSAGE_FLAG_SIGNED : 0),
                                         silc_client->hash,
					 (unsigned char *)&msg[4], len);
	print_out(c->silc->channel_name, "%s* %s %s", silc_client->sign ? 
                  "[S] " : "", silc_client->conn->local_entry->nickname, 
                  &msg[4]);
      }
    }
    /* file */
    else if (!strncmp("file ", &buf[1], 5)){
      Transfer *t;
      if (!strncmp("list", &buf[6], 4)){
	if(!transfers)
	  print_out("", "-!- No file transfers in the list");
	else
	  print_out("", "-?!- Number       User                Type            Status");
	for (t = transfers; t; t = t->next) {
	  char *status;
	  if (t->status == FILE_STATUS_WAIT)
	    status="waiting";
	  else if (t->status == FILE_STATUS_RUN)
	    status="running";
	  else if (t->status == FILE_STATUS_ERR)
	    status="error";
	  else if (t->status == FILE_STATUS_COMP)
	    status="complete";
	  print_out("", "-?!-    %-5d    %-10s          %-10s        %-10s", 
                    t->number, t->silc->nickname,
		    t->type == FILE_TYPE_IN ? "incoming" : "outgoing", status);
	}
      }
      else if(!strncmp("accept ", &buf[6], 7)) {
	DEBUG("File: ACCEPT");
	int num = atoi(&buf[13]);
	for(t = transfers; t; t = t->next)
	  if(num == t->number)
	    break;
	if(!t) {
	  print_out("", "-!- No such File transfer");
	  return;
	}
        char *path;
        silc_asprintf(&path, "%s/", getenv("HOME"));
	create_dirtree(path);

        SilcClientConnectionParams params;
        memset(&params, 0, sizeof(params));
        params.local_ip = silc_localip();
        params.no_authentication = TRUE;

	int res = silc_client_file_receive(silc_client->client, silc_client->conn,
                                           &params, silc_client->pubkey, 
                                           silc_client->privkey, file_transfer, 
                                           NULL, path, t->id, NULL, NULL);
	if(res != SILC_CLIENT_FILE_OK){
	  print_out("", "-!- Error: Couldn't start filetransfer");
	  return;
	}
      }
      else if(!strncmp("send ", &buf[6], 5)) {
	DEBUG("File: SENT");
	char **argv;
	SilcUInt32 *argv_lens, *argv_types, argc, id;
	SilcClientEntry client;
	int no_bind = FALSE;
	silc_parse_command_line((unsigned char *)buf, (void *)&argv, 
                                &argv_lens, &argv_types, &argc, 5);
	if (argc < 4) {
	  print_out("", "-!- File: /file send <nick> <path>");
	  return;
	}
	if (argc == 5)
	  if (!strcmp("-no-listener", argv[4]))
	      no_bind = TRUE;
	client = silc_client_get_client_entry(argv[2]);
	if(!silc_file_size(argv[3])) {
	  print_out("", "-!- File: File doesn't exists or is 0 bytes long");
	  return;
	}
        SilcClientConnectionParams params;
        memset(&params, 0, sizeof(params));
        if (! no_bind)
          params.local_ip = silc_localip();
        params.no_authentication = TRUE;

	int res = silc_client_file_send(silc_client->client, silc_client->conn, 
                                        client, &params, silc_client->pubkey,
                                        silc_client->privkey, file_transfer, NULL, 
                                        argv[3], &id);
	if(res != SILC_CLIENT_FILE_OK){
	  print_out("", "-!- Error: Couldn't start filetransfer");
	  return;
	}
	add_transfer(transfer_count, id, client, FILE_TYPE_OUT);
	print_out("","-!- File: transfer request sent to %s for %s", 
                  client->nickname, argv[3]);
	transfer_count++;
      }
      else if(!strncmp("close ", &buf[6], 6)) {
	DEBUG("File: CLOSE");
	int num = atoi(&buf[12]);
	for(t = transfers; t; t = t->next)
	  if(num == t->number){
	    silc_client_file_close(silc_client->client, silc_client->conn,
                                   t->id);
	    print_out("", "-!- File: file transfer closed %d", t->number);
	    rm_transfer(t);
	    return;
	  }
	print_out("", "-!- No such File transfer");
      }
    }
    /* names */
    else if (!strncmp("names", &buf[1], 5)){
      DEBUG("Input: NAMES");
      if (! strlen(c->silc->channel_name))
	return;/* Master Channel */

      print_users(c->silc);
    }
    /* Starting a new Query */
    else if (!strncmp("query ", &buf[1], 6)){
      char *name;
      SilcClientEntry recv;
      name = &buf[7];
      recv = silc_client_get_client_entry(name);
      if(!recv)
	return;
      add_query(recv);
    }
    /* Messages */
    else if (!strncmp("msg ", &buf[1], 4)) {
      DEBUG("Input: PRIVATE MESSAGE");
      char *name, *tmp;
      SilcClientEntry recv;
      name = &buf[5];
      msg = strchr(name, ' ');
      if (!msg) {
	print_out(c->silc->channel_name, 
                  "-!- Error: /msg <nickname[@server]> <message>");
	return;
      }
      msg[0] = 0;
      if(!++msg){
	print_out(c->silc->channel_name, 
                  "-!- Error: /msg <nickname[@server]> <message>");
	return;
      }
      if(!msg || ! strlen(msg)){
	  print_out(c->silc->channel_name, 
                    "-!- Error: /msg <nickname[@server]> <message>");
	  return;
      }
      recv = silc_client_get_client_entry(name);
      if(!recv) {
	print_out(c->silc->channel_name, 
                  "-!- Error: No such user");
	return;
      }
      LocalToUtf8(msg, &tmp);

      if (!silc_client_send_private_message(silc_client->client, 
                                            silc_client->conn, recv,
                                            SILC_MESSAGE_FLAG_UTF8
                                            | (silc_client->sign ? 
                                               SILC_MESSAGE_FLAG_SIGNED : 0),
                                            silc_client->hash,
					    (unsigned char *)tmp, strlen(tmp))){
	print_out("", "-!- Error: Couldn't send private message");
      }
      else {
	print_out("", ">%s< %s", recv->nickname, tmp);
      }
      free(tmp);
      return;
    }
    else if (!strncmp("set ", &buf[1], 4)) {
      /* Set Variabeles */
      if (!strncmp("signed ", &buf[5], 7)) {
	if (!strncmp("on", &buf[12], 2)){
	  silc_client->sign = TRUE;
	  print_out(c->silc->channel_name, "-!- Signing Messages: on");
	}
	else if (!strncmp("off", &buf[12], 3)){
	  silc_client->sign = FALSE;
	  print_out(c->silc->channel_name, "-!- Signing Messages: off");
	}
	else
	  print_out(c->silc->channel_name, "-!- Error: False Argument");
      }
      else if (!strncmp("signal_detach ", &buf[5], 14)) {
	if (!strncmp("on", &buf[19], 2)){
	  silc_client->detach_on_signal = TRUE;
	  print_out(c->silc->channel_name, "-!- Detach on Signal: on");
	}
	else if (!strncmp("off", &buf[19], 3)){
	  silc_client->detach_on_signal = FALSE;
	  print_out(c->silc->channel_name, "-!- Detach on Signal: off");
	}
	else
	  print_out(c->silc->channel_name, "-!- Error: False Argument");
      }
      else if (!strncmp("auto_away ", &buf[5], 10)) {
	if (!strncmp("on", &buf[15], 2)){
	  silc_client->auto_away = TRUE;
	  print_out(c->silc->channel_name, "-!- Auto away: on");
	}
	else if (!strncmp("off", &buf[15], 3)){
	  silc_client->auto_away = FALSE;
	  print_out(c->silc->channel_name, "-!- Auto away: off");
	}
	else
	  print_out(c->silc->channel_name, "-!- Error: False Argument");
      }
      else
	print_out(c->silc->channel_name, "-!- Error: False Argument");
    }

    /* MIME command */
    else if (!strncmp("mime ", &buf[1], 5)) {
      unsigned int i, b = FALSE;
      char **argv;
      SilcUInt32 *argv_lens, *argv_types, argc;
      silc_parse_command_line((unsigned char *)buf,
			      (void *)&argv, &argv_lens,
			      &argv_types, &argc, 4);
      if (argc < 2) {
	print_out(c->silc->channel_name, "-!- Error: /mime "
                  "<mimetype> [-buffer]");
	return;
      }
      for (i = 2; i < argc; i++) {
	if (!strcmp("-buffer", argv[i]))
	  b = TRUE;
      }

      si_open_mime(c, CHANNEL, argv[1], b);
      return;
    }

    else {
      DEBUG("Input: SENT_COMMAND: %s", &buf[1]);
      silc_client_command_call(silc_client->client, silc_client->conn, 
                               &buf[1], NULL);
    }
  }

  else {
    if(strlen(msg)!=0) {

      if (strlen(c->silc->channel_name)!=0) {
	DEBUG("Input: SENT");
	last_input = time(NULL);
	if(silc_client->conn->local_entry->mode & SILC_UMODE_GONE
	   || silc_client->conn->local_entry->mode & SILC_UMODE_INDISPOSED)
	  silc_client_command_call(silc_client->client, silc_client->conn, 
                                   "umode -gi", NULL);
	silc_client_send_channel_message(silc_client->client, 
                                         silc_client->conn, c->silc,
                                         NULL, SILC_MESSAGE_FLAG_UTF8
					 | (silc_client->sign ? 
                                            SILC_MESSAGE_FLAG_SIGNED : 0),
					 silc_client->hash,
                                         (unsigned char *)msg, len);
	/* Write it to the Channel */
	SilcChannelUser cuser;
	cuser = silc_client_on_channel(c->silc, silc_client->conn->local_entry);
	char *str = silc_client_chumode_char(cuser->mode);
	print_out(c->silc->channel_name, "%s<%s%s> %s",
		  silc_client->sign ? "[S] " : "",
		  str ? str : " ",
		  silc_client->conn->local_entry->nickname,
		  msg);
	free(str);
      }
    }
  }
  free(msg);
}

/* }}} */
/* {{{ proc_queries_input */

void 
proc_queries_input (Query *peer, char *buf ) 
{
  char *msg;
  char *path;
  LocalToUtf8((char *)buf, &msg);
  silc_asprintf(&path, "query/%s", peer->silc->nickname);
  if ((strlen(buf) == 2 && !strcmp("/l", buf)) || (strlen(buf)>2 && !strncmp("/l ", buf, 3))) {
    /* Close the query */
    print_out(path, "-!- You closed the query");
    rm_query(peer);
  }

  /* MIME command */
  else if (! strncmp("mime ", &buf[1], 5)) {
    unsigned int i, b = FALSE;
    char **argv;
    SilcUInt32 *argv_lens, *argv_types, argc;
    silc_parse_command_line((unsigned char *)buf,
			    (void *)&argv, &argv_lens,
			    &argv_types, &argc, 4);
    if (argc < 2) {
      print_out(path, "-!- Error: /mime <mimetype> [-buffer]");
      return;
    }
    for (i = 2; i < argc; i++) {
      if (!strcmp("-buffer", argv[i]))
	b = TRUE;
    }

    si_open_mime(peer, QUERY, argv[1], b);
    return;
  }

  else {
    if (!silc_client_send_private_message(silc_client->client, 
                                          silc_client->conn, peer->silc,
					  SILC_MESSAGE_FLAG_UTF8 
                                          | (silc_client->sign ? 
                                             SILC_MESSAGE_FLAG_SIGNED : 0),
                                          silc_client->hash,
					  (unsigned char *)msg, strlen(msg))){
      print_out(path,  "-!- Error: Couldn't send private message");
    }
    /* Write it to the Query */
    print_out(path, "<%s> %s", silc_client->conn->local_entry->nickname, msg);
  }
  free(msg);
  silc_free(path);
  DEBUG("Query: SENT");
}

/* }}} */
/* {{{ proc_watch */

void 
proc_watch(SilcClientEntry client, SilcUInt32 mode, SilcNotifyType type)
{
  char *str;
  switch(type) {
  case SILC_NOTIFY_TYPE_UMODE_CHANGE:
    str = silc_client_umode(mode);
    add_buddy(client);
    print_out("", "-!- Watch: %s new user mode is: %s", client->nickname, str);
    free(str);
    break;
  case SILC_NOTIFY_TYPE_KILLED:
    rm_buddy(client);
    print_out("", "-!- Watch: %s was killed from the network", client->nickname);
    break;
  case SILC_NOTIFY_TYPE_SIGNOFF:
    rm_buddy(client);
    print_out("", "-!- Watch: %s quit from network", client->nickname);
    break;
  case SILC_NOTIFY_TYPE_SERVER_SIGNOFF:
    rm_buddy(client);
    print_out("", "-!- Watch: %s was quit from network trough a server signoff", client->nickname);
    break;
  case SILC_NOTIFY_TYPE_NONE:
    add_buddy(client);
    print_out("", "-!- Watch: %s is online now", client->nickname);
    break;
  default:
    DEBUG("Error: Unimplemented Watch Type: %d", type);
  }
  refresh_buddylist();
}

/* }}} */
/* {{{ print_out */
static void 
print_out(char *channel, char *buf, ...)
{
        static char outfile[256];
        FILE *out;
        static char buft[8];
        time_t t = time(0);
        char *tmp = NULL;
        char *msg;
        va_list va;
        va_start(va, buf);

        create_filepath(outfile, sizeof(outfile), channel, "out");
        out = fopen(outfile, "a");
        silc_vasprintf(&tmp, buf, va);
        Utf8ToLocal(tmp ? tmp : "", &msg);

        strftime(buft, sizeof(buft), "%R ", localtime(&t));
        /* Output */
        fprintf(out, "%s%s\n", buft, msg);
        fclose(out);
        free(msg);
        silc_free(tmp);
        va_end(va);
}
/* }}} */
/* {{{ get_scc_name_by_id ( Server, Channel, Client )*/

char *
get_scc_name_by_id(int id, void *record) 
{
  char *str;
  SilcClientEntry sender;
  SilcChannelEntry channel;
  SilcServerEntry server;
  if (id == SILC_ID_CLIENT) {
    sender = (SilcClientEntry) record;
    silc_asprintf(&str, "%s", sender->nickname);
  }
  else if (id == SILC_ID_SERVER) {
    server = (SilcServerEntry)record;
    silc_asprintf(&str, "%s (Server)", server->server_name);
  }
  else {
    channel=(SilcChannelEntry)sender;
    silc_asprintf(&str, "%s (Channel)", channel->channel_name);
  }
  return str;
}

/* }}} */
/* {{{ silc_client_umode */

char *
silc_client_umode(SilcUInt32 mode) 
{
  char *stat = emalloc(15);
  stat[0] = 0;
  if (mode & SILC_UMODE_GONE)
    strcat(stat,"G");
  if (mode & SILC_UMODE_INDISPOSED)
    strcat(stat,"I");
  if (mode & SILC_UMODE_BUSY)
    strcat(stat,"B");
  if (mode & SILC_UMODE_PAGE)
    strcat(stat,"P");
  if (mode & SILC_UMODE_HYPER)
    strcat(stat,"H");
  if (mode & SILC_UMODE_SERVER_OPERATOR)
    strcat(stat,"Os");
  if (mode & SILC_UMODE_ROUTER_OPERATOR)
    strcat(stat,"Or");
  if (mode & SILC_UMODE_BLOCK_PRIVMSG)
    strcat(stat,"Np");
  if (mode & SILC_UMODE_DETACHED)
    strcat(stat,"D");
  if (mode & SILC_UMODE_REJECT_WATCHING)
    strcat(stat,"Nw");
  if (mode & SILC_UMODE_BLOCK_INVITE)
    strcat(stat,"Ni");
  if (mode & SILC_UMODE_ROBOT)
    strcat(stat,"R");
  if (mode & SILC_UMODE_ANONYMOUS)
    strcat(stat,"?");
  if (!strlen(stat))
    strcat(stat,"A");

  return stat;
}

/* }}} */
/* {{{ silc_verify_message */
char *
silc_verify_message(SilcClientEntry sender, SilcMessagePayload message) 
{
  char *retval;
  silc_asprintf(&retval, "[?] ");

  const unsigned char *pk_data;
  SilcUInt32 pk_datalen;
  unsigned int i;
  char *fpKey;
  SilcPublicKey pk = silc_message_signed_get_public_key(message, &pk_data,
                                                        &pk_datalen);
  if (!pk) {
    if(sender->fingerprint[0]){
      fpKey = silc_fingerprint(sender->fingerprint, 
                               sizeof(sender->fingerprint));
    }
    else
      return retval;
  }
  else
     fpKey = silc_hash_fingerprint(NULL, pk_data, pk_datalen);

  char *fp = emalloc(strlen(fpKey) + 1);
  for (i = 0; i < strlen(fpKey); i++){
    if (fpKey[i]==' ')
      fp[i]='_';
    else
      fp[i]=fpKey[i];
  }
  fp[i]=0;
  silc_free(fpKey);

  DIR *dir;
  char *path;
  silc_asprintf(&path, "%s/.silc/clientkeys", (char *)getenv("HOME"));
  create_dirtree(path);
  dir = opendir(path);
  struct dirent *entry;
  while((entry = readdir(dir))) {
    if(!strncmp(entry->d_name, fp, strlen(fp))){
      silc_free(path);
      silc_asprintf(&path, "%s/.silc/clientkeys/%s", 
                    (char *)getenv("HOME"), entry->d_name);
      silc_free(fp);
      SilcPublicKey cached_pk = NULL;

      /* try to load the file */
      if (!silc_pkcs_load_public_key(path, &cached_pk))
	if(!pk)
	  return retval;
      if(cached_pk){
	if(pk)
	  silc_pkcs_public_key_free(pk);
	pk = cached_pk;
      }
      /* the public key is now in pk, our "level of trust" in ret */
      if ((pk) && silc_message_signed_verify(message, pk, silc_client->hash)
          != SILC_AUTH_OK)
	sprintf(retval, "[F] ");
      else
	sprintf(retval, "[S] ");
      if (pk)
	silc_pkcs_public_key_free(pk);
      return retval;
    }
  }
  silc_free(fp);
  return retval;
}
/* }}} */
/* {{{ callback_get_client_entry */

void 
callback_get_client_entry(SilcClient client,
                          SilcClientConnection conn,
                          SilcStatus status,
                          SilcDList clients,
                          void *context)
{
  if (! silc_dlist_count(clients)) {
    print_out("", "-!- Error: No such User");
    resolve_name = (void *) 1;
    return;
  }
  if (silc_dlist_count(clients) > 1) {
    /* Find client entry */
    clients = silc_client_get_clients_local(silc_client->client, 
                                            silc_client->conn,
					    (char *)context, 
                                            FALSE);
    if(! silc_dlist_count(clients)){
      print_out("", "-!- Error: No such User");
      resolve_name = (void *)1;
      return;
    }
  }
  silc_dlist_start(clients);
  resolve_name = silc_dlist_get(clients);
  return;
}

/* }}} */
/* {{{ silc_client_get_client_entry */
SilcClientEntry 
silc_client_get_client_entry(const char *nick) 
{
  char *nickname;
  SilcDList clients;
  /* Find client entry */
  clients = silc_client_get_clients_local(silc_client->client, 
                                          silc_client->conn,
					  nick, FALSE);
  resolve_name = NULL;
  if (clients) {
    if (! silc_dlist_count(clients)) {
      /* Resolv the name */
      silc_client_get_clients(silc_client->client,
                              silc_client->conn,
                              nickname,NULL,
                              callback_get_client_entry, 
                              (char *)nick);
      while(!resolve_name)
        run_once();
      if ((int) resolve_name == 1)
        return NULL;
    }
    else {
      silc_dlist_start(clients);
      resolve_name = silc_dlist_get(clients);
    }
  }
  return resolve_name;
}
/* }}} */
/* {{{ file_transfer */
void 
file_transfer(SilcClient client,
              SilcClientConnection conn,
              SilcClientMonitorStatus status,
              SilcClientFileError error,
              SilcUInt64 offset,
              SilcUInt64 filesize,
              SilcClientEntry client_entry,
              SilcUInt32 session_id,
              const char *filepath,
              void *context) 
{
  Transfer *t;
  for (t=transfers; t; t=t->next)
    if( t->id == session_id )
      break;
  if ( offset && filesize )
    t->percent = (float) ((double)offset / (double)filesize) * (double)100.0;

  if (status == SILC_CLIENT_FILE_MONITOR_ERROR) {
    if (error == SILC_CLIENT_FILE_NO_SUCH_FILE)
      print_out("", "-!- Error: There is no such file (Number: %d)",t->number);
    else if(error == SILC_CLIENT_FILE_PERMISSION_DENIED)
      print_out("", "-!- Error: Permission denied (Number: %d)",t->number);
    else
      print_out("", "-!- Error: File transfer failed (Number: %d)",t->number);
    t->status=FILE_STATUS_ERR;
    return;
  }

  else if (status == SILC_CLIENT_FILE_MONITOR_KEY_AGREEMENT)
    print_out("", "-!- File: Negotiating keys for file transfer %d", t->number);
  else if (status == SILC_CLIENT_FILE_MONITOR_SEND  || status == SILC_CLIENT_FILE_MONITOR_RECEIVE) {
    if(offset==0) {
      t->starttime=time(NULL);
      print_out("", "-!- File: Started file transfer with %d", t->number);
      t->status=FILE_STATUS_RUN;
    }
    else if (offset == filesize) {
      unsigned long delta=time(NULL)-t->starttime;
      float kps;

      if (delta)
	kps = (double)((offset / (double)delta) + 1023) / (double)1024;
      else
	kps = (double)(offset + 1023) / (double)1024;
      print_out("", "-!- File: Completed file transfer %d [%.2f kB/s]", t->number, kps);
      if(status== SILC_CLIENT_FILE_MONITOR_RECEIVE)
	print_out("", "-!- File: saved to: %s", filepath);
      t->status=FILE_STATUS_COMP;
    }
  }


}
/* }}} */
/* {{{ run_once */

void 
run_once() 
{
  /* Select the different fifo's */
  Channel *c;
  Query *q;
  int r, maxfd=0, i;
  fd_set rd;
  struct timeval tv;
  if (silc_client->auto_away){
    if ((time(NULL) - last_input) >= 600){
      if(! (silc_client->conn->local_entry->mode & SILC_UMODE_INDISPOSED)){
	silc_client_command_call(silc_client->client, silc_client->conn, 
                                 "umode +i", NULL);
	last_input = time(NULL);
      }
    }
    if ((time(NULL) - last_input) >= 1200){
      if(! (silc_client->conn->local_entry->mode & SILC_UMODE_GONE)){
	silc_client_command_call(silc_client->client, silc_client->conn, 
                                 "umode +g", NULL);
	last_input = time(NULL);
      }
    }
  }
  FD_ZERO(&rd);
  for(c = channels; c; c = c->next) {
    if(maxfd < c->fd)
      maxfd = c->fd;
    FD_SET(c->fd, &rd);
  }
  for(q = queries; q; q = q->next) {
    if(maxfd < q->fd)
      maxfd = q->fd;
    FD_SET(q->fd, &rd);
  }
  for(i = 0; i < MAX_CONNS; i++){
    if(!buddy_conns[i])
      continue;
    if(maxfd < buddy_conns[i])
      maxfd = buddy_conns[i];
    FD_SET(buddy_conns[i], &rd);
  }
  tv.tv_sec = 0;
  tv.tv_usec = 1;
  r = select(maxfd + 1, &rd, 0, 0, &tv);
  if(r < 0) {
    if(errno == EINTR)
      return;
    perror("si: error on select()");
    exit(EXIT_FAILURE);
  }
  for(c = channels; c; c = c->next)
    if(FD_ISSET(c->fd, &rd))
      handle_channels_input(c);
  for(q = queries; q; q = q->next)
    if(FD_ISSET(q->fd, &rd))
      handle_query_input(q);
  for(i=0; i<MAX_CONNS; i++){
    if(! buddy_conns[i])
      continue;
    if(FD_ISSET(buddy_conns[i], &rd))
      si_handle_buddy_conn_input(buddy_conns[i]);
      }
  /* Run the Silc Client one Time */
  silc_client_run_one(silc_client->client);
}

/* }}} */
/* {{{ run */

int 
run(void)
{
  int fd;
  time_t conn_timeout;
  SilcClientParams params;
  SilcClientConnectionParams conn_params;
  silc_client = emalloc(sizeof(*silc_client));
  /* ClientParams */
  memset(&params, 0, sizeof(params));
  strcat(params.nickname_format, "%n@%h%a");


  silc_client->client = silc_client_alloc(&ops, &params, silc_client, NULL);
  NULL_TEST_ARG(silc_client->client, 1);

  /* Set Signed Messages per Default to off */
  silc_client->sign = FALSE;
  silc_client->auto_away = FALSE;
  last_input = time(NULL);

  /* Now we initialize the client. */
  if (!silc_client_init(silc_client->client, silc_get_username(), 
                        silc_net_localhost(),
                        realname, NULL, NULL)) {
    perror("si: Could not init SILC client");
    return 1;
  }
  /* run silc client one time to initialize it */
  silc_client_run_one(silc_client->client);

  silc_hash_alloc((const unsigned char *)"sha1", &silc_client->hash);

  /* Load Key's from $HOME/.silc/ */
  char *silc_prv;
  char *silc_pub;
  
  silc_asprintf(&silc_prv, "%s/.silc/private_key.prv", (char *)getenv("HOME"));
  silc_asprintf(&silc_pub, "%s/.silc/public_key.pub", (char *)getenv("HOME"));
  if (!silc_load_key_pair(silc_pub, silc_prv, "", &silc_client->pubkey, 
                          &silc_client->privkey)) {
    /* Generate new Key's */
    fprintf(stdout, "si: Key pair does not exist, generating it.\n");
    if (!silc_create_key_pair("rsa", 2048, silc_pub, silc_prv, NULL, "",
                              &silc_client->pubkey, &silc_client->privkey,
                              FALSE)) {
      perror("si: Could not generated key pair");
      return 1;
    }
  }
  silc_free(silc_prv);
  silc_free(silc_pub);

  /* create path */
  silc_asprintf(&ircdir, "%s/%s", prefix, host);
  create_dirtree(ircdir);

  /* Maser Channel */
  SilcChannelEntry master;
  master = calloc(1, sizeof(*master));
  NULL_TEST_ARG(master, 1);

  master->channel_name = "";
  add_channel(master);

  /* Restoring Detached data */
  char *filename;
  silc_asprintf(&filename, "%s/.silc/session.%s", getenv("HOME"), host);

  memset((void *) &conn_params, 0, sizeof(conn_params));
  conn_params.detach_data_len = 0;
  conn_params.detach_data = (unsigned char *)
    silc_file_readfile(filename, &conn_params.detach_data_len);
  silc_free(filename);
  conn_params.pfs = TRUE;
  conn_params.nickname = username;

  printf("%s %s\n", host, username);
  /* Start connecting to server.  This is asynchronous connecting so the
     connection is actually created later after we run the client. */
  if(! silc_client_connect_to_server(silc_client->client, &conn_params, 
                                     silc_client->pubkey, silc_client->privkey,
                                     host, port, silc_connection_cb, NULL)){
    fprintf(stderr, "si: Couldn't connect to server\n");
    exit(EXIT_FAILURE);
  }


  /* Run it one time */
  conn_timeout = time(NULL); 
  while(!connected) {
    silc_client_run_one(silc_client->client);
    if ((time(NULL) - conn_timeout ) > 15) {
      fprintf(stderr, "si: Couldn't connect to server (Timeout)\n");
      exit(EXIT_FAILURE);
    }
  }

  /* Install the SigHandler for SIGINT */
  signal(SIGINT, sig_handler);
  signal(SIGTERM, sig_handler);

  /* Ignore some SIGPIPE */
  signal(SIGPIPE, SIG_IGN);

  /* Run the init Script */
  if(init_script){
    fd=open(init_script, 0);
    if(fd==-1)
      perror("si: Couldn't open init script, continuing..");
    else {
      Channel *c;
      char buf[4097];
      char buf2[4098];
      /* find master channel */
      for(c = channels; c; c = c->next)
	if(!strlen(c->silc->channel_name))
	  break;
      while(read_line(fd, 4096, buf)!=-1){
	snprintf(buf2, 4097, "/%s", buf);
	proc_channels_input(c, buf2);
      }
    }
  }
  /* Init the Buddylist */
  init_buddylist();

  for (;;) {
    run_once();
  }

  silc_client_free(silc_client->client);
  free(silc_client);
  return 0;
}

/* }}} */

/* {{{ silc_channel_message */
static void
silc_channel_message(SilcClient client, SilcClientConnection conn,
		     SilcClientEntry sender, SilcChannelEntry channel,
		     SilcMessagePayload payload,
		     SilcChannelPrivateKey key,
		     SilcMessageFlags flags, const unsigned char *content,
		     SilcUInt32 message_len)
{
  /* Write it out*/
  SilcChannelUser cuser;
  cuser=silc_client_on_channel(channel, sender);
  char *str;
  char *sigstat = NULL;

  if(flags & SILC_MESSAGE_FLAG_SIGNED){
    sigstat=silc_verify_message(sender,payload);
  }

  if (flags & SILC_MESSAGE_FLAG_DATA) {
    /* Process MIME message */
    SilcMime mime = silc_mime_decode(NULL, content, message_len);
    si_process_mime(client, conn, sender, channel, payload, key, flags,
		    mime, FALSE);
    return;
  }

  str = silc_client_chumode_char(cuser->mode);
  if (flags & SILC_MESSAGE_FLAG_ACTION)
    print_out(channel->channel_name, "%s* %s %s", sigstat?sigstat:"",sender->nickname, (char *)content);
  else
    print_out(channel->channel_name, "%s<%s%s> %s", sigstat?sigstat:"", str?str:" ", sender->nickname, (char *)content);
  free(str);
}
/* }}} */
/* {{{ silc_command */
static void
silc_command(SilcClient client, SilcClientConnection conn, bool success,
             SilcCommand command, SilcStatus status, SilcUInt32 argc,
             unsigned char **argv)
{
  /* If error occurred in client library with our command, print the error */
  if (status != SILC_STATUS_OK && command != SILC_COMMAND_WATCH){
    print_out("", "-!- Error: %s: %s",
	     silc_get_command_name(command),
	     silc_get_status_message(status));
  }
}
/* }}} */
/* {{{ silc_command_reply */
static void
silc_command_reply(SilcClient client, SilcClientConnection conn,
		   SilcCommand command, SilcStatus status, SilcStatus error,
                   va_list va)
{
  SilcUInt32 mode, count;
  SilcChannelEntry channel;
  SilcClientEntry sender;
  SilcServerEntry server;
  SilcHashTableList ch;
  SilcChannelUser cuser;
  Channel *c;
  char *str, *str2;
  int type;
  /* If error occurred in client library with our command, print the error */
  if (SILC_STATUS_IS_ERROR(status) && command != SILC_COMMAND_WATCH)  {
      print_out("", "-!- Error %s: %s",
		silc_get_command_name(command),
		silc_get_status_message(error));
      return;
    }

  /* Check for successful JOIN */
  if (command == SILC_COMMAND_JOIN) {
    DEBUG("Command: JOIN");
    (void)va_arg(va, SilcClientEntry);
    channel = va_arg(va, SilcChannelEntry);
    add_channel(channel);
    print_users(channel);
  }
  else if (command == SILC_COMMAND_UMODE) {
    mode=va_arg(va, SilcUInt32);
    char *stat=silc_client_umode(mode);
    print_out("", "-!- You changed your user mode to: %s", stat?stat:"");
    free(stat);

  }
  else if (command == SILC_COMMAND_LEAVE) {
    DEBUG("Command: LEAVE");
    channel = va_arg(va, SilcChannelEntry);
    print_out("", "-!- You have left the Channel");
    rm_channel(channel->context);
  }
  else if (command == SILC_COMMAND_USERS) {
    DEBUG("Command: USERS");
    channel = va_arg(va, SilcChannelEntry);
    int joined=FALSE;
    for (c = channels; c; c = c->next)
      if (!strcmp(c->silc->channel_name, channel->channel_name))
	joined = TRUE;
    print_out(joined ? channel->channel_name : "", "-!- Users: ");
    silc_hash_table_list(channel->user_list, &ch);
    while (silc_hash_table_get(&ch, (void *)&sender, (void *)&cuser)) {
      char *stat, *mode;
      if (!sender->nickname)
	continue;
      stat = silc_client_umode(sender->mode);
      mode = silc_client_chumode_char(cuser->mode);
      print_out(joined ? channel->channel_name: "" , "-!- %s %s%s %s@%s[%s]",
                sender->nickname, stat ,mode ? mode : "",
                sender->username, sender->hostname,
		sender->realname ? sender->realname : "");
      free(mode);
      free(stat);
    }
    silc_hash_table_list_reset(&ch);
  }
  else if (command == SILC_COMMAND_NICK) {
    DEBUG("Command: NICK");
    sender = va_arg(va, SilcClientEntry);
    for (c=channels; c; c=c->next){
      print_out(c->silc->channel_name, "-!- Your new nickname is %s", sender->nickname);
    }
  }
  else if (command == SILC_COMMAND_WHOIS) {
    DEBUG("Command: WHOIS");
    int len = 2;
    char *nickname, *username, *realname, *str=malloc(1), *fingerprint;
    SilcDList channels;
    SilcUInt32 *user_modes;
    SilcUInt32 mode, idle;
    NULL_TEST(str);
    str[0] = 0;
    sender = va_arg(va, SilcClientEntry);
    nickname = va_arg(va, char *);
    username = va_arg(va, char *);
    realname = va_arg(va, char *);
    channels = va_arg(va, SilcDList);
    mode = va_arg(va, SilcUInt32);
    idle = va_arg(va, SilcUInt32);
    fingerprint = va_arg(va, char *);
    user_modes = va_arg(va, SilcUInt32 *);
    /* Username etc.*/
    print_out("", "-?!- Whois: %s@%s (%s)", sender->nickname_normalized, 
              sender->server, username);
    print_out("", "-?!- Nickname: %s (%s)", sender->nickname_normalized, 
              sender->nickname);
    print_out("", "-?!- Realname: %s", realname);
    /* Channels */
    if (channels && user_modes) {
      int i = 0;

      silc_dlist_start(channels);
      SilcChannelPayload channel;
      while ((channel = silc_dlist_get(channels)) != SILC_LIST_END) {
        SilcUInt32 name_len;
        char *m = silc_client_chumode_char(user_modes[i++]);
        char *name = (char *)silc_channel_get_name(channel, &name_len);
        if (m)
          len += strlen(m);
        len += name_len + 2;
        str = realloc(str, len);
        NULL_TEST(str);
        strcat(str, name);
        if(m)
          strcat(str, m);
        strcat(str, " ");
        silc_free(m);
      }
      print_out("", "-?!- Channels: %s", str);
      silc_free(str);
    }
    /* User Mode */
    if(mode){
      str=silc_client_umode(mode);
      print_out("", "-?!- User Mode: %s", str);
      free(str);
    }
    /* Idle */
    if(idle) {
      print_out("", "-?!- Idle: %d %s", idle>60?idle/60:idle, idle>60?"minutes":"seconds");
    }
    /* Fingerprint */
    if (fingerprint) {
      fingerprint = silc_fingerprint((unsigned char *)fingerprint, 20);
      print_out("", "-?!- Fingerprint: %s", fingerprint);
      free(fingerprint);
    }
  print_out("", "");
  }
  else if(command == SILC_COMMAND_LIST) {

    channel = va_arg(va, SilcChannelEntry);
    if (!channel){
      print_out("", "-!- Error: There is no such channel on the Network");
      return;
    }
    (void)va_arg(va, void *);
    str = va_arg(va, char *);
    count = va_arg(va, SilcUInt32);
    DEBUG("Status: %d", status);
    if(status == SILC_STATUS_LIST_START || status==0 )
      print_out("", "Channel                 Users            Topic");
    print_out("", "%-25s %-15d %s", channel->channel_name, count, str?str:"");
  }
  else if(command == SILC_COMMAND_INFO) {
    DEBUG("Command: INFO");
    (void)va_arg(va, void *);
    str=va_arg(va, char *);
    str2=va_arg(va, char *);
    print_out("","-!- Info: %s %s", str, str2);
  }
  else if(command == SILC_COMMAND_STATS) {
    DEBUG("Comman: STATS");
    SilcClientStats *stats;
    const char *tmptime;
    int days, hours, mins, secs;

    stats = va_arg(va, SilcClientStats *);

    tmptime = silc_time_string(stats->starttime);
    days = stats->uptime / (24 * 60 * 60);
    stats->uptime -= days * (24 * 60 * 60);
    hours = stats->uptime / (60 * 60);
    stats->uptime -= hours * (60 * 60);
    mins = stats->uptime / 60;
    stats->uptime -= mins * 60;
    secs = stats->uptime;

    /* Output */
    print_out("", "Starttime: %-20s Uptime: %d days %d hours %d "
              "minutes %d seconds", tmptime, days, hours, mins, secs);
    print_out("", "Local Clients:          %-5d  Local Channels:         %-5d",
              stats->my_clients, stats->my_channels);
    print_out("", "Local Server Operators: %-5d  Local Router Operators: %-5d",
              stats->my_server_ops, stats->my_router_ops);
    print_out("", "Cell Clients:           %-5d  Cell Channels:          %-5d"
              "Cell Servers: %-5d", stats->cell_clients, stats->cell_channels, 
              stats->cell_servers);
    print_out("", "Total Clients:          %-5d  Total Channels:         %-5d",
              stats->clients, stats->channels);
    print_out("", "Total Servers:          %-5d  Total Routers:          %-5d", 
              stats->servers, stats->routers);
    print_out("", "Total Server Operators: %-5d  Total Router Operators: %-5d", 
              stats->server_ops, stats->router_ops);
  }
  else if(command==SILC_COMMAND_GETKEY) {
    DEBUG("Command: GETKEY");
    SilcPublicKey key;
    void *entry;
    char *filename, *pos;
    unsigned int i;

    type=va_arg(va, int);
    entry=va_arg(va, void *);
    key=va_arg(va, SilcPublicKey);
    unsigned char *pk;
    SilcUInt32 pk_len;
    pk = silc_pkcs_public_key_encode(key, &pk_len);
    char *fpKey = silc_hash_fingerprint(0, pk, pk_len);
    char *fp=malloc(strlen(fpKey)+1);
    NULL_TEST(fp);
    for (i = 0; i < strlen(fpKey); i++){
      if (fpKey[i] == ' ')
	fp[i]='_';
      else
	fp[i]=fpKey[i];
    }
    fp[i]=0;
    free(fpKey);

    if(type == SILC_ID_SERVER) {
      server = (SilcServerEntry)entry;
      /* $HOME/.silc/clientkeys/fp(name):server */
      silc_asprintf(&filename, "%s/.silc/incoming_keys/%s(%s):server", 
                    getenv("HOME"), fp, server->server_name);
    }
    else if(type== SILC_ID_CLIENT) {
     sender=(SilcClientEntry)entry;
     silc_asprintf(&filename, "%s/.silc/incoming_keys/%s(%s)", 
                   getenv("HOME"), fp, sender->nickname);
    }
    pos = strrchr(filename, '/');
    pos[0] = 0;
    create_dirtree(filename);
    pos[0] = '/';
    if(!silc_pkcs_save_public_key(filename, key, SILC_PKCS_FILE_BASE64)){
      print_out("", "-!- Error: Couldn't save public key");
      return;
    }
    print_out("", "-!- Getkey: The Key you had asked, was saved in: %s", filename);
    silc_free(filename);
  }
  else if(command == SILC_COMMAND_WHOWAS) {
    (void)va_arg(va, SilcClientEntry);
    char *nickname = va_arg(va, char *);
    char *username = va_arg(va, char *);
    char *realname = va_arg(va, char *);
    print_out("", "-?!- Whowas: %s (%s) [%s]", nickname, username, 
              realname ? realname : "");
  }
  else if(command == SILC_COMMAND_OPER)
    print_out("", "-!- You are server oper now");
  else if(command == SILC_COMMAND_SILCOPER)
    print_out("", "-!- You are router oper now");
  else if(command == SILC_COMMAND_DETACH) {
    SilcBuffer detach = va_arg(va, SilcBuffer);
    char *file;
    silc_asprintf(&file, "%s/.silc/session.%s", getenv("HOME"), host);
    silc_file_writefile(file, (char *)silc_buffer_data(detach),
                        silc_buffer_len(detach));
    silc_free(file);
  }

  va_end(va);
}
/* }}} */
/* {{{ silc_connection_cb */
void 
silc_connection_cb(SilcClient si,
                        SilcClientConnection conn,
                        SilcClientConnectionStatus status,
                        SilcStatus error,
                        const char *message,
                        void *context) 
{
  SilcChannelEntry channel;
  Silc client = si->application;
  Query *q;
  Channel *c;

  switch(status) {
  case SILC_CLIENT_CONN_SUCCESS_RESUME:
    DEBUG("Connect: RESUME");
    SilcHashTableList ch;
    silc_hash_table_list(conn->local_entry->channels, &ch);
    while (silc_hash_table_get(&ch, (void *)&channel, NULL))
      add_channel(channel);
    silc_hash_table_list_reset(&ch);

    char *filename;
    silc_asprintf(&filename, "%s/.silc/session.%s", getenv("HOME"), host);
    unlink(filename);
    free(filename);
    print_out("", "-!- Resumed old session");
  case SILC_CLIENT_CONN_SUCCESS:
    client->conn = conn;
    connected=1;
    DEBUG("Connect: Connection succsessful");
    break;  
  case SILC_CLIENT_CONN_DISCONNECTED:
    /* We got disconnected from server */
    for (c = channels; c; c = c->next)
      print_out(c->silc->channel_name, "-!- You have left Network [%s]",
                message ? message : "");
    for (q = queries; q; q = q->next)
      print_out(c->silc->channel_name, "-!- You have left Network [%s]", 
                message ? message : "");
    DEBUG("Disconnected: %s:%s", silc_get_status_message(status),
          message ? message : "");
    exit(0);
    break;
  case SILC_CLIENT_CONN_ERROR:
    print_out("", "Couldn't connect: unknown error");
    goto error;
  case SILC_CLIENT_CONN_ERROR_KE:
    print_out("", "Couldn't connect: key exchange failed");
    goto error;
  case SILC_CLIENT_CONN_ERROR_AUTH:
    print_out("", "Couldn't connect: authenitication failed");
    goto error;
  case SILC_CLIENT_CONN_ERROR_RESUME:
    print_out("", "Couldn't connect: resume error");
    goto error;
  case SILC_CLIENT_CONN_ERROR_TIMEOUT:
    print_out("", "Couldn't connect: timeout error");
error:
    fprintf(stderr, "si: Could not connect to server\n");
    silc_client_close_connection(si, conn);
    exit(EXIT_FAILURE);
  }
}
/* }}} */
/* {{{ silc_notify */

static void
silc_notify(SilcClient client, SilcClientConnection conn,
	    SilcNotifyType type, ...)
{
  char *str, *str2;
  int id;
  SilcUInt32 mode;
  SilcChannelEntry channel, channel2;
  SilcClientEntry sender, sender2;
  Channel *c;
  Query *q;
  SilcHashTableList ch;
  va_list va;

  va_start(va, type);

  switch (type) {
  case SILC_NOTIFY_TYPE_NONE:
    /* Received something that we are just going to dump to screen. */
    str = va_arg(va, char *);
    print_out("", str);
    break;
  case SILC_NOTIFY_TYPE_JOIN:
    /* Joined Channel*/
    DEBUG("Notify: JOIN");
    sender = va_arg(va, SilcClientEntry);
    channel = va_arg(va, SilcChannelEntry);
    print_out(channel->channel_name, "-!- %s [%s@%s] has joined %s", sender->nickname, sender->username,sender->hostname, channel->channel_name);
    break;
  case SILC_NOTIFY_TYPE_LEAVE:
    /* Left Channel */
    DEBUG("Notify: LEAVE");
    sender = va_arg(va, SilcClientEntry);
    channel = va_arg(va, SilcChannelEntry);
    print_out(channel->channel_name, "-!- %s [%s@%s] has left %s", sender->nickname, sender->username,sender->hostname, channel->channel_name);
    break;
  case SILC_NOTIFY_TYPE_INVITE:
    DEBUG("Notify: INVITE");
    channel = va_arg(va, SilcChannelEntry);
    char *name=va_arg(va, char *);
    sender = va_arg(va, SilcClientEntry);
    print_out("", "-!- You were invited joining the channel %s by %s", channel?channel->channel_name:name, sender->nickname);
    break;
  case SILC_NOTIFY_TYPE_SERVER_SIGNOFF:
    DEBUG("Notify: SERVER SIGNOFF");
    SilcDList clients;

    (void)va_arg(va, void *);
    clients = va_arg(va, SilcDList);
    silc_dlist_start(clients);

    while((sender = silc_dlist_get(clients))) {
      silc_hash_table_list(sender->channels, &ch);
      while (silc_hash_table_get(&ch, (void *)&channel, (void *)&channel2)) {
	for(c = channels; c; c = c->next)
	  if(!strcmp(c->silc->channel_name, channel->channel_name))
	    print_out(channel->channel_name,"-!- %s  has quit [%s]", 
                      sender->nickname, "server signoff");
      }
      silc_hash_table_list_reset(&ch);
      for (q=queries; q; q=q->next){
	char *path;
	silc_asprintf(&path, "query/%s", sender->nickname);
	print_out(path,"-!- %s  has quit [%s]", sender->nickname, str?str:"");
	silc_free(path);
      }
    }
    break;
  case SILC_NOTIFY_TYPE_SIGNOFF:
    /* Left the Network */
    DEBUG("Notify: SIGNOFF");
    sender = va_arg(va, SilcClientEntry);
    rm_buddy(sender);
    refresh_buddylist();
    str = va_arg(va, char *);
    silc_hash_table_list(sender->channels, &ch);
    while (silc_hash_table_get(&ch, (void *)&channel, (void *)&channel2)) {
      for(c=channels; c; c=c->next)
	if(!strcmp(c->silc->channel_name, channel->channel_name))
	  print_out(channel->channel_name,"-!- %s  has quit [%s]", 
                    sender->nickname, str ? str : "");
    }
    silc_hash_table_list_reset(&ch);
    for (q = queries; q; q = q->next){
      char *path;
      silc_asprintf(&path, "query/%s", sender->nickname);
      print_out(path,"-!- %s  has quit [%s]", sender->nickname, str?str:"");
      silc_free(path);
    }
    break;
  case SILC_NOTIFY_TYPE_TOPIC_SET:
    /* Changed topic.*/
    DEBUG("Notify: TOPIC");
    id = va_arg(va, int);
    sender = va_arg(va, void *);
    str2 = va_arg(va, char *);
    channel = va_arg(va, SilcChannelEntry);
    str=get_scc_name_by_id(id, sender);
    print_out(channel->channel_name, "-!- %s has changed topic to: %s", str?str:"", str2);
    free(str);
    break;
  case SILC_NOTIFY_TYPE_NICK_CHANGE:
    /* Somebody has changed his name */
    DEBUG("Notify: NICK_CHANGE");
    char *old_nickname;
    sender = va_arg(va, SilcClientEntry);
    old_nickname = va_arg(va, char *);

    silc_hash_table_list(sender2->channels, &ch);
    while (silc_hash_table_get(&ch, (void *)&channel, (void *)&channel2)) {
      for(c=channels; c; c=c->next)
	if(!strcmp(c->silc->channel_name, channel->channel_name))
	  print_out(channel->channel_name,  "-!- %s is known as: %s", 
                    old_nickname, sender->nickname);
    }
    silc_hash_table_list_reset(&ch);
    break;
  case SILC_NOTIFY_TYPE_MOTD:
    /* Received the Message of the Day from the server. */
    DEBUG("Notify: MOTD");
    str = va_arg(va, char *);
    print_out("", str);
    break;
  case SILC_NOTIFY_TYPE_CUMODE_CHANGE:
    /* ChannelUserMode Change*/
    DEBUG("Notify: CUMODE CHANGE");
    id = va_arg(va, int);
    sender = va_arg(va, void *);
    mode = va_arg(va, SilcUInt32);
    sender2 = va_arg(va, SilcClientEntry);
    channel = va_arg(va, SilcChannelEntry);
    str = get_scc_name_by_id(id, sender);
    str2 = silc_client_chumode(mode);
    print_out(channel->channel_name, "-!- channel user mode/%s/%s [%s] by %s", 
              channel->channel_name, sender2->nickname, str2 ? 
              str2 : "removed all", str ? str : "");
    silc_free(str);
    silc_free(str2);
    break;
  case SILC_NOTIFY_TYPE_CMODE_CHANGE:
    DEBUG("Notify:  CMODE CHANGE");
    id = va_arg(va, int);
    sender = va_arg(va, void *);
    mode = va_arg(va, SilcUInt32);
    (void)va_arg(va, char *);                  /* cipher */
    (void)va_arg(va, char *);                  /* hmac */
    (void)va_arg(va, char *);                  /* passphrase */
    (void)va_arg(va, SilcPublicKey);           /* founder key */
    (void)va_arg(va, SilcDList);
    channel = va_arg(va, SilcChannelEntry);

    str = get_scc_name_by_id(id, sender);
    str2 = silc_client_chmode(mode, channel->cipher, channel->hmac);
    print_out(channel->channel_name, "-!- channel mode/%s [%s] by %s",
              channel->channel_name, str2 ? str2 : "removed all", 
              str ? str : "");
    silc_free(str);
    silc_free(str2);
    break;
  case SILC_NOTIFY_TYPE_KICKED:
    DEBUG("Notify: KICKED");
    sender = va_arg(va, SilcClientEntry);
    str = va_arg(va, char *);
    sender2 = va_arg(va, SilcClientEntry);
    channel = va_arg(va, SilcChannelEntry);
    if (sender == silc_client->conn->local_entry) {
      /* The User was kicked */
      print_out(channel->channel_name, "-!- You were kicked by %s [%s]", 
                sender2->nickname ? sender2->nickname : "anonymous",
                str ? str : "");
      for (c = channels; c; c = c->next)
	if (!strcmp(c->silc->channel_name, channel->channel_name))
	  rm_channel(c);
    }
    else {
      print_out(channel->channel_name, "-!- %s was kicked by %s [%s]", 
                sender->nickname, sender2->nickname ? sender2->nickname 
                : "anonymous", str ? str : "");
    }
    break;
  case SILC_NOTIFY_TYPE_KILLED:
    DEBUG("Notify: KILLED");
    sender = va_arg(va, SilcClientEntry);
    rm_buddy(sender);
    refresh_buddylist();
    str = va_arg(va, char *);
    id = va_arg(va, int);
    sender2 = va_arg(va, void *);
    str2 = get_scc_name_by_id(id, sender2);
    if (sender == silc_client->conn->local_entry) {
      /* The User was killed */
      for (c = channels; c; c = c->next)
	print_out(c->silc->channel_name, "-!- You were killed from the Silc "
                  "Network by %s [%s]", str2 ? str2 : "anonymous", str);
      for (q = queries; q; q = q->next){
	char *path;
	silc_asprintf(&path, "query/%s", sender->nickname);
	print_out(path, "-!- You were killed from the Silc Network by %s [%s]",
                  str2 ? str2 : "anonymous", str);
	silc_free(path);
      }
      exit(0);
    }
    else {
      silc_hash_table_list(sender->channels, &ch);
      while (silc_hash_table_get(&ch, (void *)&channel, (void *)&channel2)) {
	for(c = channels; c; c = c->next)
	  if(!strcmp(c->silc->channel_name, channel->channel_name))
	    print_out(channel->channel_name, "-!- %s was killed from the "
                      "Silc Network by %s [%s]", sender->nickname, 
                      str2 ? str2 : "anonymous", str);

      }
      silc_hash_table_list_reset(&ch);

      for (q = queries; q; q = q->next)
	if(SILC_ID_CLIENT_COMPARE(&sender->id, &q->silc->id)){
	  char *path;
	  silc_asprintf(&path, "query/%s", sender->nickname);
	  print_out(path,  "-!- %s was killed from the Silc Network by %s [%s]",
                    sender->nickname, str2 ? str2 : "anonymous", str);
	  silc_free(path);
	}
    }
  case SILC_NOTIFY_TYPE_WATCH:
    DEBUG("Notify: WATCH");
    sender = va_arg(va, SilcClientEntry);
    str = va_arg(va, char *);
    mode = va_arg(va, SilcUInt32);
    SilcNotifyType watch_type=va_arg(va, int);
    if (watch_type == SILC_NOTIFY_TYPE_NICK_CHANGE){
      add_buddy(sender);
      if (str)
	print_out("", "-!- Watch: %s is known as %s", sender->nickname, str);
      else
	print_out("", "-!- Watch: %s is online now", sender->nickname, str);

      va_end(va);
      refresh_buddylist();
      return;
    }
    proc_watch(sender, mode, watch_type);
    break;
  default:
    printf("TYPE: %d\n", type);
    /* Ignore rest */
    break;
  }

  va_end(va);
}

/* }}} */
/* {{{ silc_private_message */

static void
silc_private_message(SilcClient client, SilcClientConnection conn,
		     SilcClientEntry sender, SilcMessagePayload payload,
		     SilcMessageFlags flags,
		     const unsigned char *msg,
		     SilcUInt32 message_len)
{

  Query *q;
  char *path = NULL;
  char *sigstat = NULL;
  for (q=queries; q; q=q->next)
    if (SILC_ID_CLIENT_COMPARE(&sender->id, &q->silc->id)){
      /* We have an open query */
      silc_asprintf(&path, "query/%s", sender->nickname);
      break;
    }
  if(flags & SILC_MESSAGE_FLAG_SIGNED)
    sigstat=silc_verify_message(sender, payload);

  if (flags & SILC_MESSAGE_FLAG_DATA) {
    /* Process MIME message */
    SilcMime mime = silc_mime_decode(NULL, msg, message_len);
    si_process_mime(client, conn, sender, NULL, payload, NULL, flags,
		    mime, FALSE);
    free(path);
    return;
  }

  print_out(path ? path : "",  "%s<%s> %s", sigstat ? sigstat : "",
            sender->nickname, (char *)msg);
  free(path);
}

/* }}} */
/* {{{ verify_public_key */
int 
verify_public_key(SilcPublicKey key, int remote) 
{
  unsigned char *pk;
  SilcUInt32 pk_len;
  char *fp, *path;
  unsigned int tmp;
  DIR *dir;
  struct dirent *entry;
  pk = silc_pkcs_public_key_encode(key, &pk_len);
  fp = silc_hash_fingerprint(NULL, pk, pk_len);
  for (tmp = 0; tmp < strlen(fp); tmp++)
    if(fp[tmp] == ' ')
      fp[tmp]='_';
  silc_asprintf(&path, "%s/.silc/%s", (char *)getenv("HOME"), 
                remote == SERVER ? "serverkeys" : "clientkeys");
  create_dirtree(path);
  dir = opendir(path);
  silc_free(path);

  while((entry = readdir(dir))) {
    if(!strncmp(entry->d_name, fp, strlen(fp))){
      silc_asprintf(&path, "%s/.silc/%s/%s", (char *)getenv("HOME"), 
                    remote == SERVER ? "serverkeys" : "clientkeys", 
                    entry->d_name);
      /* Load key */
      SilcPublicKey cached_pk = NULL;
      if (!silc_pkcs_load_public_key(path, &cached_pk)) {
	fprintf(stderr, "Couldn't load file: %s\n", path);
	return KEY_UNKOWN;
	break;
      }
      if(silc_pkcs_public_key_compare(key, cached_pk)){
	silc_free(path);
	silc_free(fp);
	return KEY_TRUSTED;
      }
      else {
	silc_free(path);
	silc_free(fp);
	return KEY_INVALID;
      }
    }
  }
  closedir(dir);
  silc_free(fp);
  return KEY_UNKOWN;
}
/* }}} */
/* {{{ silc_verify_public_key */
static void
silc_verify_public_key(SilcClient client, SilcClientConnection conn,
		       SilcConnectionType conn_type, 
                       SilcPublicKey pkey, 
		       SilcVerifyPublicKey completion, void *context)
{
  char *fingerprint, *path, *pos; 
  unsigned char *pk;
  unsigned int tmp;
  SilcUInt32 pk_len;

  pk = silc_pkcs_public_key_encode(pkey, &pk_len);
  fingerprint = silc_hash_fingerprint(NULL, pk, pk_len);

  for (tmp = 0; tmp < strlen(fingerprint); tmp++)
    if(fingerprint[tmp]==' ')
      fingerprint[tmp]='_';

  if (conn_type == SILC_CONN_SERVER || conn_type == SILC_CONN_ROUTER){
    int res = verify_public_key(pkey, SERVER);
      if (res == KEY_TRUSTED){
	DEBUG("Verify: TRUSTED");
	completion(TRUE, context);
        silc_free(pk);
	return;
      }
      else if ( res == KEY_INVALID ){
	print_out("", "-!- Error: The cached public key is not the same as the sent one!");
	printf("ERROR: INVALID KEY; PANIC\n");
	exit(1);
      }
      else {
	/* Save the Public Key */
	silc_asprintf(&path, "%s/.silc/incoming_keys/%s(%s):server",
                      getenv("HOME"), fingerprint, conn->remote_host);
	pos = strrchr(path, '/');
	pos[0] = 0;
	create_dirtree(path);
	pos[0] = '/';
	if(!silc_pkcs_save_public_key(path, pkey, SILC_PKCS_FILE_BASE64)){
	  fprintf(stderr, "Couldn't save Public Key\n");
	  exit(1);
	}
	printf("The server's public key you wanted to connect is unkown!\n");
	printf("Name: %s\n", silc_pkcs_get_name(pkey));
	printf("Host: %s; Port: %d\n", conn->remote_host, conn->remote_port);
	printf("si has saved his public key to:\n");
	printf("%s\n", path);
	printf("\nIf you trust this key move it to ~/.silc/serverkeys, but don't rename it\n");
	exit(1);
      }
  }
  else {
    /* Clients */
    int res = verify_public_key(pkey, CLIENT);
    if (res == KEY_TRUSTED){
      DEBUG("Verify: TRUSTED");
      completion(TRUE, context);
    }
    else if (res == KEY_INVALID) {
      print_out("", "-!- Error: It seems as if the saved key (%s) is poisoned", fingerprint);
      completion(FALSE, context);
    }
    else {
      print_out("", "-!- Error: You had to trust the key first (%s)", fingerprint);
      completion(FALSE, context);
    }
    silc_free(pk);
  }
}
/* }}} */
/* {{{ silc_ftp */
static void
silc_ftp(SilcClient client, SilcClientConnection conn,
	 SilcClientEntry client_entry, SilcUInt32 session_id,
	 const char *hostname, SilcUInt16 port)
{
  DEBUG("Ftp: REQUEST");
  add_transfer(transfer_count, session_id, client_entry, FILE_TYPE_IN);
  if(hostname && port)
    print_out("","-!- %s would like to send you a file (Number %d) [%s:%d]", client_entry->nickname, transfer_count, hostname, port);
  else
    print_out("","-!- %s would like to send you a file (Number %d)", client_entry->nickname, transfer_count);
  transfer_count++;
}
/* }}} */

/* {{{ Empty and unused, but required Functions */
static void
silc_get_auth_method(SilcClient client, SilcClientConnection conn,
		     char *hostname, SilcUInt16 port,
                     SilcAuthMethod method,
		     SilcGetAuthMeth completion,
		     void *context)
{
  /* MyBot assumes that there is no authentication requirement in the
     server and sends nothing as authentication.  We just reply with
     TRUE, meaning we know what is the authentication method. :). */
  completion(SILC_AUTH_NONE, NULL, 0, context);
}

static void
silc_say(SilcClient client, SilcClientConnection conn,
	 SilcClientMessageType type, char *msg, ...)
{
  va_list va;
  va_start(va, msg);
  char *message;
  silc_vasprintf(&message, msg, va);
  print_out("", "-!- %s", message);
  silc_free(message);
}

static void
silc_ask_passphrase(SilcClient client, SilcClientConnection conn,
		    SilcAskPassphrase completion, void *context)
{
  completion(NULL, 0, context);
}

void
silc_key_agreement(SilcClient client, SilcClientConnection conn,
		   SilcClientEntry client_entry, const char *hostname,
		   SilcUInt16 protocol, SilcUInt16 port)
{
}
/* }}} */

/* {{{ Client-Ops and main() */
SilcClientOperations ops = {
  silc_say,
  silc_channel_message,
  silc_private_message,
  silc_notify,
  silc_command,
  silc_command_reply,
  silc_get_auth_method,
  silc_verify_public_key,
  silc_ask_passphrase,
  silc_key_agreement,
  silc_ftp,
};

int main(int argc, char **argv)
{
  char c;
  int do_fork = 0;

  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"version", 0, 0, 'v'},
      {"help", 0, 0, 'h'},
      {"silc-dir", 1, 0, 'i'},
      {"server", 1, 0, 's'},
      {"port", 1, 0, 'p'},
      {"nick", 1, 0, 'n'},
      {"fullname", 1, 0, 'f'},
      {"init-script", 1, 0, 'x'},
      {"debug", 0, 0, 'd'},
      {"fork", 0, 0, 'F'}, 
      {0, 0, 0, 0}
    };

    c = getopt_long (argc, argv, "dFx:i:s:p:n:f:vh",
		     long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {
    case 'v': version(); break;
    case 'h': usage(); break;
    case 'i': prefix=strdup(optarg); break;
    case 's': host = strdup(optarg); break;
    case 'p': port = atoi(optarg); break;
    case 'n': username = strdup(optarg); break;
    case 'f': realname = strdup(optarg); break;
    case 'x': init_script = strdup(optarg); break;
    case 'F': do_fork = 1; break;
    case 'd': debug = 1; break;
    default: usage(); break;
    }
  }

  if(!strlen(prefix)){
    prefix=malloc(strlen(getenv("HOME"))+5);
    NULL_TEST_ARG(prefix,1);
    sprintf(prefix, "%s/irc", getenv("HOME"));
  }

  if (!username)
    username = silc_get_username();

  if(do_fork) {
    pid_t p = fork();

    if(p < 0) {
      /* fork failed */
      perror("si");
      return 1;
    }

    if(p) {
      /* parent process */
      return 0;
    }

    /* child process, we're a daemon, therefore close terminal fds */
    close(0);
    close(1);
    close(2);
  }

  /* Run the Mainloop */
  return run();
}

/* }}} */
