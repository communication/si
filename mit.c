/* MIT/X Consortium License

* (C)opyright MMV Anselm R. Garbe <garbeam@wmii.de>
* (C)opyright MMV Nico Golde <nico at ngolde dot de>

* Permission is hereby granted, free of charge, to any person obtaining a
* copy of this software and associated documentation files (the "Software"),
* to deal in the Software without restriction, including without limitation
* the rights to use, copy, modify, merge, publish, distribute, sublicense,
* and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/



static char *
lower(char *s) 
{
	char *p;
	for(p = s; p && *p; p++) *p = tolower(*p);
	return s;
}

/* creates directories top-down, if necessary */
static void 
create_dirtree(const char *dir) 
{
	char tmp[256];
	char *p;
	size_t len;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	len = strlen(tmp);
	if(tmp[len - 1] == '/')
		tmp[len - 1] = 0;
	for(p = tmp + 1; *p; p++)
		if(*p == '/') {
			*p = 0;
			mkdir(tmp, S_IRWXU);
			*p = '/';
		}
	mkdir(tmp, S_IRWXU);
}

static int 
get_filepath(char *filepath, size_t len, char *channel, char *file)
{
	if(strlen(channel)!=0) {
		if(!snprintf(filepath, len, "%s/%s", ircdir, lower(channel)))
			return 0;
		create_dirtree(filepath);
		return snprintf(filepath, len, "%s/%s/%s", ircdir,lower(channel), file);
	}
	return snprintf(filepath, len, "%s/%s", ircdir, file);
}

static void 
create_filepath(char *filepath, size_t len, char *channel, char *suffix)
{
	if(!get_filepath(filepath, len, channel, suffix)) {
		fprintf(stderr, "%s", "ii: path to irc directory too long\n");
		exit(EXIT_FAILURE);
	}
}

static int 
open_channel(char *name)
{
	static char infile[256];
	create_filepath(infile, sizeof(infile), name, "in");
	if(access(infile, F_OK) == -1)
		mkfifo(infile, S_IRWXU);
	return open(infile, O_RDONLY | O_NONBLOCK, 0);
}

static int
read_line(int fd, size_t res_len, char *buf)
{
	size_t i = 0;
	char c;
	do {
	  if(read(fd, &c, sizeof(char)) != sizeof(char))
	    return -1;
	  buf[i++] = c;
	}
	while(c != '\n' && i < res_len);
	buf[i - 1] = 0;			/* eliminates '\n' */
	return 0;
}

static void 
rm_channel(Channel *c)
{
  Channel *p;
  if(channels == c) {
    c->silc->context = NULL;
    channels = channels->next;
  } else
    for(p = channels; p; p = p->next)
      if(p->next == c) {
	p->silc->context = NULL;
	p->next = c->next;
      }
  free(c);
}

static void 
handle_channels_input(Channel *c)
{
  static char buf[PIPE_BUF];

  if (read_line(c->fd, PIPE_BUF, buf) == -1) {
    int fd = open_channel(c->silc->channel_name);
    if(fd != -1)
      c->fd = fd;
    else
      rm_channel(c);
    return;
  }
  proc_channels_input(c, buf);
}

static void 
add_channel(SilcChannelEntry channel)
{
  Channel *c;
  int fd;

  if (channel->context)
    return; /* already handled */

  fd = open_channel(channel->channel_name);
  if(fd == -1) {
    perror("si: cannot create in channel");
    return;
  }
  c = calloc(1, sizeof(*c));
  NULL_TEST_ARG(c, exit(1));
  if(!channels)
    channels = c;
  else {
    c->next = channels;
    channels = c;
  }
  c->fd = fd;
  c->silc = channel;
  c->silc->context = c;
}

void 
rm_query(Query *peer) 
{
  Query *q;
  if (peer == queries) {
    peer->silc->context = NULL;
    queries = queries->next;
  }
  else
    for (q = queries; q; q = q->next)
      if (SILC_ID_CLIENT_COMPARE(&peer->silc->id, 
                                 &q->next->silc->id)) {
	q->silc->context = NULL;
	q->next = peer->next;
      }
  free(peer);
}

static void handle_query_input(Query *peer)
{
  static char buf[PIPE_BUF];
  char *path;

  if(read_line(peer->fd, PIPE_BUF, buf) == -1) {
    path=malloc(strlen(peer->silc->nickname)+8);
    NULL_TEST(path);
    int fd = open_channel(path);
    if(fd != -1)
      peer->fd = fd;
    else
      rm_query(peer);
    return;
  }
  proc_queries_input(peer, buf);
}

void 
add_query(SilcClientEntry peer) 
{
  Query *q;
  int fd;
  char *path;

  if (peer->context)
    return; /* Already handled */

  
  silc_asprintf(&path, "query/%s", peer->nickname);
  fd = open_channel(path);
  if(!fd){
    perror("si: Couldn't create Query");
    return;
  }
  q = calloc(1, sizeof(Query));
  NULL_TEST_ARG(q, exit(1));
  /* Write to the query */
  print_out(path, "-!- You opened the query");
  if (!queries) {
    queries = q;
  }
  else {
    q->next = queries;
    queries = q;
  }
  q->silc = peer;
  q->fd = fd;
  q->silc->context = q;

  silc_client_ref_client(silc_client->client, silc_client->conn, peer);
}

void 
add_transfer(int num, SilcUInt32 id, SilcClientEntry sender, int type) 
{
  Transfer *t, *tmp;
  t = calloc(1, sizeof(Transfer));
  NULL_TEST(t);
  if(!transfers)
    transfers = t;
  else{
    tmp = transfers;
    while(tmp->next)
      tmp = tmp->next;
    tmp->next = t;
  }
  t->number = num;
  t->id = id;
  t->silc = sender;
  t->type = type;
  t->status = FILE_STATUS_WAIT;
}

void 
rm_transfer(Transfer *t) 
{
  Transfer *tmp;
  if (t == transfers) {
    transfers = transfers->next;
  }
  else
    for (tmp = transfers; tmp; tmp = tmp->next)
      if(tmp->next->id == t->id)
	tmp->next = t->next;
  free(t);
}
